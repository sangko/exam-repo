Feature: Three for Two Deal for any item

Scenario: User meets 3 for 2 Deal requirements

    Given A 3 for 2 deal on 'UNLIMITED_1GB'. So for example, if you buy 3 Unlimited 1GB Sims, you will pay the price of 2 only for the 1st month
    When user adds 3 'UNLIMITED_1GB' (3 for 2 deal item) and 1 'UNLIMITED_5GB' to cart
    Then user only pays 94.70 for 3 x 'UNLIMITED_1GB' and 1 x 'UNLIMITED_5GB' under 3 for 2 deal
    