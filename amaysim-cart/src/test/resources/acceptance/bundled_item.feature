Feature: Bundled Item

Scenario: User meets bundled item requirement

	Given we will bundle in a free 1 'DATA_PACK_1GB' free-of-charge with every '1' 'UNLIMITED_2GB' sold
	When user adds 1 'UNLIMITED_1GB' and 2 'UNLIMITED_2GB' (with bundled item) to cart
	Then user only pays 84.70 for 1 x 'UNLIMITED_1GB' and 2 x 'UNLIMITED_2GB' plus a bundled 2 X 'DATA_PACK_1GB'
	