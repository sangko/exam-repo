Feature: Discount Across the Board

Scenario: User meets Across the Board requirement

	Given adding the promo code 'I<3AMAYSIM' will apply a 10% discount across the board
	When user adds 1 'DATA_PACK_1GB' (1 GB Data-pack) and 1 'UNLIMITED_1GB' (Unlimited 1 GB) to cart and applies 'I<3AMAYSIM' promotion code
	Then user only pays 31.32 for 1 x 'UNLIMITED_1GB' and 1 x 'DATA_PACK_1GB' because of applied promotion code
	