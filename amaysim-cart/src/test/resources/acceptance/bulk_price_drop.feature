Feature: Bulk Price Drop

Scenario: User meets bulk price drop requirement

	Given the 'UNLIMITED_5GB' will have a bulk discount applied; whereby the price will drop to 39.90 each for the 1st month, if the customer buys more than 3
	When user adds 4 'UNLIMITED_5GB' and 2 'UNLIMITED_1GB' to cart
	Then user only pays 209.40 for 4 x 'UNLIMITED_5GB' and 2 x 'UNLIMITED_1GB' under price drop deal
	