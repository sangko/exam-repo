package acceptance;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Assert;

import acceptance.util.Catalogue;
import acceptance.util.ItemFactory;
import acceptance.util.PromotionManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ecomm.cart.AmaysimShoppingCart;
import ecomm.cart.Item;
import ecomm.cart.ShoppingCart;
import ecomm.rule.PromotionRule;

public class Scenario3 {

	private ShoppingCart cart;
	private PromotionRule bundledItemRule;

	@Given("we will bundle in a free (\\d+) '(.+)' free-of-charge with every '(\\d+)' '(.+)' sold$")
	public void priceDropScenario(int bundledItemQuantity, String bundledItem, int minimumQuantity,
			String itemWithBundledItem) {
	
		// initialize promotion rule
		String appliesTo = Catalogue.valueOf(itemWithBundledItem).getProduct().getCode();
		Item itemToBundle = ItemFactory.getItem(bundledItem);

		bundledItemRule = PromotionManager.getBundledRule(appliesTo, minimumQuantity, bundledItemQuantity, itemToBundle);

		// initialize cart with promotion rule
		cart = new AmaysimShoppingCart(Arrays.asList(bundledItemRule));

	}

	@When("user adds (\\d+) '(.+)' and (\\d+) '(.+)' \\(with bundled item\\) to cart$")
	public void userAddsToCart(int regularItemQuantity, String regularItem, int withBundledItemQuantity,
			String withBundledItem) {
		// add selected items to cart
		cart.add(ItemFactory.getItem(withBundledItem, withBundledItemQuantity));
		cart.add(ItemFactory.getItem(regularItem, regularItemQuantity));
	}

	@Then("user only pays (.+) for (\\d+) x '(.+)' and (\\d+) x '(.+)' plus a bundled (\\d+) X '(.+)'$")
	public void totalAndItems(String amount, int regularItemQuantity, String regularItem, int withBundledItemQuantity,
			String withBundledItem, int bundledItemQuantity, String bundledItem) {

		// check expected amount to pay
		BigDecimal expectedAmount = new BigDecimal(amount);
		Assert.assertTrue(cart.totalIncDiscount().equals(expectedAmount));

		// get and check regular item in cart
		Item expectedRegularItem = ItemFactory.getItem(regularItem, regularItemQuantity);
		Optional<Item> regularItemInCart = cart.getItem(expectedRegularItem.getItemCode());
		Assert.assertTrue(regularItemInCart.isPresent());

		// check expected quantity of regular item in cart
		Assert.assertTrue(regularItemInCart.get().getQuantity() == expectedRegularItem.getQuantity());

		// get and check if with bundled item in cart
		Item expectedWithBundledItem = ItemFactory.getItem(withBundledItem, withBundledItemQuantity);
		Optional<Item> withBundledItemInCart = cart.getItem(expectedWithBundledItem.getItemCode());
		Assert.assertTrue(withBundledItemInCart.isPresent());

		// check expected quantity of with bundled item in cart
		Assert.assertTrue(withBundledItemInCart.get().getQuantity() == expectedWithBundledItem.getQuantity());

		// get and check bundled item in cart
		Item expectedBundledItem = ItemFactory.getItem(bundledItem, bundledItemQuantity);
		Optional<Item> expectedBundledItemInCart = withBundledItemInCart.get().getBundledItems().stream()
				.filter(i -> i.getItemCode().equals(expectedBundledItem.getItemCode())).findFirst();
		Assert.assertTrue(expectedBundledItemInCart.isPresent());

		// check expected quantity of bundled item in cart
		Assert.assertTrue(expectedBundledItemInCart.get().getQuantity() == expectedBundledItem.getQuantity());

	}

}
