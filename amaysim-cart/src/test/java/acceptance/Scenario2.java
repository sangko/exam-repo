package acceptance;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Assert;

import acceptance.util.Catalogue;
import acceptance.util.ItemFactory;
import acceptance.util.PromotionManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ecomm.cart.AmaysimShoppingCart;
import ecomm.cart.Item;
import ecomm.cart.ShoppingCart;
import ecomm.rule.PromotionRule;

public class Scenario2 {

	private ShoppingCart cart;
	private PromotionRule priceDropRule;

	@Given("the '(.+)' will have a bulk discount applied; whereby the price will drop to (.+) each for the (\\d+)st month, if the customer buys more than (\\d+)$")
	public void priceDropScenario(String priceDropItem, String priceDropAmount, int validity, int condition) {

		// initialize promotion rule
		String appliesTo = Catalogue.valueOf(priceDropItem).getProduct().getCode();
		int minQuantity = condition + 1; // more than the condition
		BigDecimal priceDrop = new BigDecimal(priceDropAmount);
		LocalDate validForAMonth = LocalDate.now().plusMonths(validity);

		priceDropRule = PromotionManager.getDropPriceRule(appliesTo, minQuantity, priceDrop, validForAMonth);

		// initialize cart with promotion rule
		cart = new AmaysimShoppingCart(Arrays.asList(priceDropRule));

	}

	@When("user adds (\\d+) '(.+)' and (\\d+) '(.+)' to cart$")
	public void userAddsToCart(int priceDropItemQuantity, String priceDropItem, int regularItemQuantity,
			String regularItem) {
		// add selected items to cart
		cart.add(ItemFactory.getItem(priceDropItem, priceDropItemQuantity));
		cart.add(ItemFactory.getItem(regularItem, regularItemQuantity));
	}

	@Then("user only pays (.+) for (\\d+) x '(.+)' and (\\d+) x '(.+)' under price drop deal$")
	public void totalAndItems(String amount, int priceDropItemQuantity, String priceDropItem, int regularItemQuantity,
			String regularItem) {

		// check expected amount to pay
		BigDecimal expectedAmount = new BigDecimal(amount);
		Assert.assertTrue(cart.totalIncDiscount().equals(expectedAmount));

		// get and check if price drop item in cart
		Item expectedPriceDropItem = ItemFactory.getItem(priceDropItem, priceDropItemQuantity);
		Optional<Item> priceDropItemInCart = cart.getItem(expectedPriceDropItem.getItemCode());
		Assert.assertTrue(priceDropItemInCart.isPresent());

		// check expected quantity of price drop item in cart
		Assert.assertTrue(priceDropItemInCart.get().getQuantity() == expectedPriceDropItem.getQuantity());

		// get and check regular item in cart
		Item expectedRegularItem = ItemFactory.getItem(regularItem, regularItemQuantity);
		Optional<Item> regularItemInCart = cart.getItem(expectedRegularItem.getItemCode());
		Assert.assertTrue(regularItemInCart.isPresent());

		// check expected quantity of regular item in cart
		Assert.assertTrue(regularItemInCart.get().getQuantity() == expectedRegularItem.getQuantity());

	}

}
