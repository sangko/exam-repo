package acceptance;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Assert;

import acceptance.util.ItemFactory;
import acceptance.util.PromotionManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ecomm.cart.AmaysimShoppingCart;
import ecomm.cart.Item;
import ecomm.cart.ShoppingCart;
import ecomm.rule.PromotionRule;

public class Scenario4 {

	private ShoppingCart cart;
	private PromotionRule discountAcrossTheBoardRule;

	@Given("adding the promo code '(.+)' will apply a (\\d+)% discount across the board$")
	public void discountAcrossTheBoardScenario(String promotionCode, int discountByPercent) {

		// initialize promotion rule
		discountAcrossTheBoardRule = PromotionManager.getDiscountByPercentRule(promotionCode, discountByPercent);

		// initialize cart with promotion rule
		cart = new AmaysimShoppingCart(Arrays.asList(discountAcrossTheBoardRule));

	}

	@When("user adds (\\d+) '(.+)' \\(1 GB Data-pack\\) and (\\d+) '(.+)' \\(Unlimited 1 GB\\) to cart and applies '(.+)' promotion code$")
	public void userAddsToCart(int regularItem1Quantity, String regularItem1, int regularItem2Quantity,
			String regularItem2, String promoCode) {
		
		// add selected items to cart
		cart.add(ItemFactory.getItem(regularItem2, regularItem2Quantity), promoCode);
		cart.add(ItemFactory.getItem(regularItem1, regularItem1Quantity), promoCode);
	}

	@Then("user only pays (.+) for (\\d+) x '(.+)' and (\\d+) x '(.+)' because of applied promotion code$")
	public void totalAndItems(String amount, int regularItem1Quantity, String regularItem1, int regularItem2Quantity,
			String regularItem2) {

		// check expected amount to pay
		BigDecimal expectedAmount = new BigDecimal(amount);
		Assert.assertTrue(cart.totalIncDiscount().equals(expectedAmount));

		// get and check regular item in cart
		Item expectedRegularItem = ItemFactory.getItem(regularItem1, regularItem1Quantity);
		Optional<Item> regularItemInCart = cart.getItem(expectedRegularItem.getItemCode());
		Assert.assertTrue(regularItemInCart.isPresent());

		// check expected quantity of regular item in cart
		Assert.assertTrue(regularItemInCart.get().getQuantity() == expectedRegularItem.getQuantity());

		// get and check if with bundled item in cart
		Item expectedWithBundledItem = ItemFactory.getItem(regularItem2, regularItem2Quantity);
		Optional<Item> withBundledItemInCart = cart.getItem(expectedWithBundledItem.getItemCode());
		Assert.assertTrue(withBundledItemInCart.isPresent());

		// check expected quantity of with bundled item in cart
		Assert.assertTrue(withBundledItemInCart.get().getQuantity() == expectedWithBundledItem.getQuantity());

	}

}
