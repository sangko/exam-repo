package acceptance;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, strict = false, plugin = { "pretty", "json:target/cucumber.json" }, tags = {
		"~@ignore" })
public class RunAmaysimShoppingCartTest {
}
