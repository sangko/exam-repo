package acceptance;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Assert;

import acceptance.util.Catalogue;
import acceptance.util.ItemFactory;
import acceptance.util.PromotionManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ecomm.cart.AmaysimShoppingCart;
import ecomm.cart.Item;
import ecomm.cart.ShoppingCart;
import ecomm.product.Product;
import ecomm.rule.PromotionRule;

public class Scenario1 {

	private ShoppingCart cart;
	private PromotionRule threeForTwoDeal;

	@Given("A 3 for 2 deal on '(.+)'. So for example, if you buy (\\d+) Unlimited 1GB Sims, you will pay the price of 2 only for the (\\d+)st month$")
	public void threeForTwoDeal(String threeForTwoItem, int threeForTwoItemQuantity, int validity) {

		// initialize promotion rule
		Product product = Catalogue.valueOf(threeForTwoItem).getProduct();

		String appliesTo = product.getCode();
		BigDecimal discount = product.getPrice();
		int minQuantity = threeForTwoItemQuantity;
		LocalDate validForAMonth = LocalDate.now().plusMonths(validity);

		threeForTwoDeal = PromotionManager.getDiscountRule(appliesTo, minQuantity, discount, validForAMonth);

		// initialize cart with promotion rule
		cart = new AmaysimShoppingCart(Arrays.asList(threeForTwoDeal));

	}

	@When("user adds (\\d+) '(.+)' \\(3 for 2 deal item\\) and (\\d+) '(.+)' to cart$")
	public void userAddsToCart(int threeForTwoItemQuantity, String threeForTwoItem, int regularItemQuantity,
			String regularItem) {

		// add selected items to cart
		cart.add(ItemFactory.getItem(threeForTwoItem, threeForTwoItemQuantity));
		cart.add(ItemFactory.getItem(regularItem, regularItemQuantity));
	}

	@Then("user only pays (.+) for (\\d+) x '(.+)' and (\\d+) x '(.+)' under 3 for 2 deal$")
	public void totalAndItems(String amount, int threeForTwoItemQuantity, String threeForTwoItem,
			int regularItemQuantity, String regularItem) {

		// check expected amount to pay
		BigDecimal expectedAmount = new BigDecimal(amount);
		Assert.assertTrue(cart.totalIncDiscount().equals(expectedAmount));

		// get and check if 3 for 2 item in cart
		Item expected3For2Item = ItemFactory.getItem(threeForTwoItem, threeForTwoItemQuantity);
		Optional<Item> threeForTwoInCart = cart.getItem(expected3For2Item.getItemCode());
		Assert.assertTrue(threeForTwoInCart.isPresent());

		// check expected quantity of 3 for 2 item in cart
		Assert.assertTrue(threeForTwoInCart.get().getQuantity() == expected3For2Item.getQuantity());

		// get and check regular item in cart
		Item expectedRegularItem = ItemFactory.getItem(regularItem, regularItemQuantity);
		Optional<Item> regularItemInCart = cart.getItem(expectedRegularItem.getItemCode());
		Assert.assertTrue(regularItemInCart.isPresent());

		// check expected quantity of regular item in cart
		Assert.assertTrue(regularItemInCart.get().getQuantity() == expectedRegularItem.getQuantity());

	}

}
