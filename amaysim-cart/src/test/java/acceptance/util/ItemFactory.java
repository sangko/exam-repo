package acceptance.util;

import ecomm.cart.Item;

public class ItemFactory {

	private ItemFactory() {
	}

	public static Item getItem(String name) {
		return new Item(Catalogue.valueOf(name).getProduct());
	}
	
	public static Item getItem(String name, int quantity) {
		return new Item(Catalogue.valueOf(name).getProduct(), quantity);
	}

}
