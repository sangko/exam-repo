package acceptance.util;

import java.math.BigDecimal;

import ecomm.product.Product;
import ecomm.product.SimCard;

public enum Catalogue {

	UNLIMITED_1GB("ult_small", "Unlimited 1GB", new BigDecimal("24.90")), 
	UNLIMITED_2GB("ult_medium", "Unlimited 2GB", new BigDecimal("29.90")), 
	UNLIMITED_5GB("ult_large", "Unlimited 5GB",	new BigDecimal("44.90")), 
	DATA_PACK_1GB("1gb", "1 GB Data-pack", new BigDecimal("9.90"));

	private Product product;

	private Catalogue(String code, String name, BigDecimal price) {
		product = new SimCard(code, name, price);
	}

	public Product getProduct() {
		return product;
	}

}
