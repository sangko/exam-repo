package acceptance.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import ecomm.cart.Item;
import ecomm.rule.BundledItemsForEveryMinimumQty;
import ecomm.rule.DiscountByPercent;
import ecomm.rule.DiscountForEveryMinimumQty;
import ecomm.rule.DropPriceForMinimumQty;
import ecomm.rule.PromotionRule;

public class PromotionManager {
	
	private PromotionManager(){}
	
	public static PromotionRule getDiscountRule(String appliesTo, int minimumQuantity, BigDecimal discount, LocalDate expiration){
		return new DiscountForEveryMinimumQty(appliesTo, minimumQuantity, discount, expiration);
	}
	
	public static PromotionRule getDropPriceRule(String appliesTo, int minimumQuantity, BigDecimal dropPrice, LocalDate expiration){
		return new DropPriceForMinimumQty(appliesTo, minimumQuantity, dropPrice, expiration);
	}

	public static PromotionRule getDiscountByPercentRule(String appliesTo, int percent){
		return new DiscountByPercent(appliesTo, percent);
	}
	
	public static PromotionRule getDiscountByPercentRule(String appliesTo, int percent, LocalDate expiration){
		return new DiscountByPercent(appliesTo, percent, expiration);
	}
	
	public static PromotionRule getBundledRule(String appliesTo, int minimumQuantity, int quantityToBundleForNQty, Item bundledItem){
		return new BundledItemsForEveryMinimumQty(appliesTo, minimumQuantity, quantityToBundleForNQty, bundledItem);
	}

	public static PromotionRule getBundledRule(String appliesTo, int minimumQuantity, int quantityToBundleForNQty, List<Item> bundledItems, LocalDate expiration){
		return new BundledItemsForEveryMinimumQty(appliesTo, minimumQuantity, quantityToBundleForNQty, bundledItems, expiration);
	}

}
