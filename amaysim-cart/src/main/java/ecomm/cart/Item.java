package ecomm.cart;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import ecomm.product.Product;

public class Item {

	private int quantity;

	private Product product;

	private List<Item> bundledItems = Collections.emptyList();

	private BigDecimal discountedSubTotal = BigDecimal.ZERO;

	private BigDecimal subTotal = BigDecimal.ZERO;

	public Item(Item item) {
		this(item.getProduct(), item.getQuantity());
	}

	public Item(Product product) {
		this(product, 1);
	}

	public Item(Product product, int qty) {

		if (qty < 1)
			throw new IllegalArgumentException("quantity cannot be less than 1");

		Objects.requireNonNull(product, "product cannot be null.");

		this.quantity = qty;
		this.product = product;

	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public BigDecimal subTotal() {
		this.subTotal = getProduct().getPrice().multiply(BigDecimal.valueOf(getQuantity())).setScale(2,
				RoundingMode.CEILING);

		return subTotal;
	}

	public List<Item> getBundledItems() {
		return new ArrayList<>(bundledItems);
	}

	public void setBundledItems(List<Item> bundledItems) {
		Objects.requireNonNull(bundledItems, "bundledItems cannot be null");

		this.bundledItems = bundledItems;
	}

	public BigDecimal getDiscountedSubTotal() {
		return discountedSubTotal.setScale(2, RoundingMode.CEILING);
	}

	public void setDiscountedSubTotal(BigDecimal discountedSubTotal) {
		Objects.requireNonNull(discountedSubTotal, "discountedSubTotal can't be null");
		this.discountedSubTotal = discountedSubTotal;
	}

	public String getItemCode() {
		return product.getCode();
	}

	public String toString() {
		String bundled = "";

		if (!bundledItems.isEmpty())
			bundled = " bundled: " + bundledItems;

		return this.getProduct().getName() + " : " + getQuantity() + bundled;
	}

}
