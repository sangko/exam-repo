package ecomm.cart;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import ecomm.rule.PromotionRule;

public class AmaysimShoppingCart implements ShoppingCart {

	private List<Item> items = new ArrayList<>();

	private BigDecimal total = BigDecimal.ZERO;

	private BigDecimal totalIncDiscount = BigDecimal.ZERO;

	private List<PromotionRule> promotionRules = Collections.emptyList();

	public AmaysimShoppingCart() {
	}

	public AmaysimShoppingCart(List<PromotionRule> promotionRules) {
		Objects.requireNonNull(promotionRules, "promotionRules can't be null.");

		this.promotionRules = promotionRules;
	}
	
	public synchronized void add(Item item) {
		add(item, null);
	}

	public synchronized void add(Item item, String promoCode) {
		Objects.requireNonNull(item, "item may not have been initiliazed properly.");

		// update item quantity or add new item to cart
		items = addItemOrUpdateQuantity(item, items);

		// apply promotional rules if any
		items = applyPromotionRules(items, promotionRules, promoCode);

		// calculate total
		total = getTotal(items);

		// calculate total including discount
		totalIncDiscount = getTotalIncDiscount(items);
	}

	public synchronized BigDecimal total() {
		return total;
	}

	public synchronized BigDecimal totalIncDiscount() {
		return totalIncDiscount;
	}

	public synchronized List<Item> items() {
		return new ArrayList<>(this.items);
	}

}
