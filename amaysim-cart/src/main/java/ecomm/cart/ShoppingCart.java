package ecomm.cart;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import ecomm.rule.PromotionRule;

public interface ShoppingCart {

	public List<Item> items();

	public void add(Item item);
	
	public void add(Item item, String promoCode);

	public BigDecimal total();

	public BigDecimal totalIncDiscount();
	
	public default Optional<Item> getItem(String productCode){
		Objects.requireNonNull(productCode, "productCode can't be null");
		
		return items().stream().filter(i -> i.getItemCode().equals(productCode))
		.findFirst();
	}

	public default List<Item> addItemOrUpdateQuantity(Item item, List<Item> items) {
		Objects.requireNonNull(item, "item to add cannot be null.");
		Objects.requireNonNull(items, "items to add to cannot be null.");

		List<Item> newItems = new ArrayList<>(items);
		Optional<Item> searchedItem = getItem(item.getItemCode());
		if (searchedItem.isPresent()) {
			Item foundItem = searchedItem.get();
			int updatedQuantity = foundItem.getQuantity() + item.getQuantity();
			foundItem.setQuantity(updatedQuantity);
		} else {	
			newItems.add(item);
		}

		return newItems;
	}

	public default List<Item> applyPromotionRules(List<Item> items, List<PromotionRule> promotionRules, String promoCode) {

		Objects.requireNonNull(items, "items to add to cannot be null.");
		Objects.requireNonNull(promotionRules, "promotionRules to add cannot be null.");

		if (!promotionRules.isEmpty()) {

			boolean tryPromotionalCode = promoCode != null && promoCode.length() > 0;
			
			promotionRules.stream().forEach(pr -> {			
				// use entered promotion code
				items.forEach(i -> {
					if(tryPromotionalCode)
						pr.promotionCode(promoCode);
					
					pr.applyTo(i);
				});
			});

			// return new items with applied promotions
			return new ArrayList<>(items);
		}

		// return regular items if no valid promotions
		return items;
	}

	public default BigDecimal getTotal(List<Item> items) {
		Objects.requireNonNull(items, "items can't be null.");

		return items.stream().map(i -> i.subTotal()).reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public default BigDecimal getTotalIncDiscount(List<Item> items) {
		Objects.requireNonNull(items, "items can't be null.");

		return items.stream().map(i -> {
			BigDecimal discountedAmount = i.getDiscountedSubTotal();
			if (discountedAmount.compareTo(BigDecimal.ZERO) > 0) {
				return discountedAmount;
			}

			return i.subTotal();
		}).reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public default void clear() {
		items().clear();
	}
	
}
