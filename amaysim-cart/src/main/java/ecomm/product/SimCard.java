package ecomm.product;

import java.math.BigDecimal;
import java.util.Objects;

public class SimCard implements Product {

	private String code;

	private String name;

	private BigDecimal price;

	public SimCard(String code, String name, BigDecimal price) {
		Objects.requireNonNull(code, "product code can't be null");
		Objects.requireNonNull(name, "product name can't be null");
		Objects.requireNonNull(price, "product price can't be null");
		
		this.code = code;
		this.name = name;
		this.price = price;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public BigDecimal getPrice() {
		return price;
	}

	@Override
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean equals(Object otherProduct) {

		if (otherProduct instanceof SimCard) {

			String otherProductCode = ((SimCard) otherProduct).getCode();
			if (getCode().equals(otherProductCode))
				return true;

		}

		return false;
	}

	public int hashCode() {
		return getCode().hashCode();
	}

}
