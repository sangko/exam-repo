package ecomm.product;

import java.math.BigDecimal;

public interface Product {

	String getCode();

	void setCode(String code);

	String getName();

	void setName(String name);

	BigDecimal getPrice();

	void setPrice(BigDecimal price);

}