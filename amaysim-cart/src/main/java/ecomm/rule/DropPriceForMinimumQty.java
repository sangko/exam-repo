package ecomm.rule;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import ecomm.cart.Item;

public class DropPriceForMinimumQty implements PromotionRule {

	private int minimumQuantity;

	private String appliesTo;

	private LocalDate expiration;

	private BigDecimal priceDrop = BigDecimal.ZERO;

	public DropPriceForMinimumQty(String appliesTo, int minimumQuantity, BigDecimal priceDrop, LocalDate expiration) {

		this.appliesTo = appliesTo;
		this.minimumQuantity = minimumQuantity;
		this.expiration = expiration;
		this.priceDrop = priceDrop;

		validate(this.appliesTo, this.minimumQuantity, this.priceDrop, this.expiration);

	}

	public void applyTo(Item item) {
		if (isValid()) {

			Objects.requireNonNull(item, "item cannot be null.");

			if (appliesTo.equals(item.getProduct().getCode())) {
				if (item.getQuantity() >= minimumQuantity) {
					BigDecimal discountedAmount = priceDrop.multiply(BigDecimal.valueOf(item.getQuantity()));
					item.setDiscountedSubTotal(discountedAmount);
				}
			}

		}
	}

	@Override
	public String getAppliesTo() {
		return appliesTo;
	}

	@Override
	public boolean isValid() {
		return LocalDate.now().isBefore(expiration);
	}

	private void validate(String appliesTo, int minimumQuantity, BigDecimal priceDrop, LocalDate expiration) {
		Objects.requireNonNull(appliesTo, "appliesTo can't be null.");
		Objects.requireNonNull(priceDrop, "priceDrop can't be null.");
		Objects.requireNonNull(expiration, "expiration can't be null.");
		
		if (priceDrop.compareTo(BigDecimal.ZERO) < 0)
			throw new IllegalStateException("price drop can't be less than 0.");
		
		if (minimumQuantity < 1)
			throw new IllegalArgumentException("minimumQuantity must be greater than 0.");

		if (!LocalDate.now().isBefore(expiration))
			throw new IllegalArgumentException("expiration must be later than today.");
	}

}
