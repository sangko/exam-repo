package ecomm.rule;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import ecomm.cart.Item;

public class BundledItemsForEveryMinimumQty implements PromotionRule {

	private int minimumQuantity;
	
	private int quantityToBundleForNQty;

	private String appliesTo;

	private LocalDate expiration;

	private List<Item> itemsToBundle = Collections.emptyList();

	// default validity of promotion expiration to 1 year if no expiration defined
	public BundledItemsForEveryMinimumQty(String appliesTo, int minimumQuantity, int quantityToBundleForNQty, Item itemToBundle) {
		this(appliesTo, minimumQuantity, quantityToBundleForNQty, Arrays.asList(itemToBundle), LocalDate.now().plusYears(1));
	}

	public BundledItemsForEveryMinimumQty(String appliesTo, int minimumQuantity, int quantityToBundleForNQty, List<Item> itemsToBundle,
			LocalDate expiration) {

		this.appliesTo = appliesTo;
		this.itemsToBundle = itemsToBundle;
		this.minimumQuantity = minimumQuantity;
		this.quantityToBundleForNQty = quantityToBundleForNQty;
		this.expiration = expiration;
		
		validate(this.appliesTo, this.minimumQuantity, this.quantityToBundleForNQty, this.itemsToBundle, this.expiration);

	}

	public void applyTo(Item item) {
		if (isValid()) {

			Objects.requireNonNull(item, "item cannot be null.");

			if (appliesTo.equals(item.getProduct().getCode())) {
				if (item.getQuantity() >= minimumQuantity) {

					itemsToBundle.stream().forEach(i -> {
						// shouldn't worry about '0' quantity, already validated
						// on item creation
						BigDecimal bundledSetQuantity = BigDecimal.valueOf(item.getQuantity() / minimumQuantity);
						BigDecimal promotionalBundledQuantity = BigDecimal.valueOf(quantityToBundleForNQty);
						i.setQuantity(bundledSetQuantity.multiply(promotionalBundledQuantity).intValue());

					});

					item.setBundledItems(itemsToBundle);
				}
			}
		}
	}

	@Override
	public String getAppliesTo() {
		return appliesTo;
	}

	@Override
	public boolean isValid() {
		return LocalDate.now().isBefore(expiration);
	}

	private void validate(String appliesTo, int minimumQuantity, int quantityToBundleForNQty, List<Item> itemsToBundle, LocalDate expiration) {
		Objects.requireNonNull(appliesTo, "appliesTo can't be null.");
		Objects.requireNonNull(itemsToBundle, "itemsToBundle can't be null.");
		Objects.requireNonNull(expiration, "expiration can't be null.");

		if (minimumQuantity < 1)
			throw new IllegalArgumentException("minimumQuantity must be greater than 0.");
		
		if (quantityToBundleForNQty < 1)
			throw new IllegalArgumentException("quantityToBundleForNQty must be greater than 0.");

		if (!LocalDate.now().isBefore(expiration))
			throw new IllegalArgumentException("expiration must be later than today.");
	}

}
