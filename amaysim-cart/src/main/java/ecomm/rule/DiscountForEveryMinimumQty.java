package ecomm.rule;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import ecomm.cart.Item;

public class DiscountForEveryMinimumQty implements PromotionRule {

	private int minimumQuantity;

	private String appliesTo;

	private LocalDate expiration;

	private BigDecimal discount = BigDecimal.ZERO;

	public DiscountForEveryMinimumQty(String appliesTo, int minimumQuantity, BigDecimal discount,
			LocalDate expiration) {

		this.appliesTo = appliesTo;
		this.minimumQuantity = minimumQuantity;
		this.expiration = expiration;
		this.discount = discount;

		validate(this.appliesTo, this.minimumQuantity, this.discount, this.expiration);

	}

	public void applyTo(Item item) {
		if (isValid()) {

			Objects.requireNonNull(item, "item cannot be null.");

			if (appliesTo.equals(item.getProduct().getCode())) {
				if (item.getQuantity() >= minimumQuantity) {
					BigDecimal accumulatedDiscount = discount.multiply(BigDecimal.valueOf(item.getQuantity() / minimumQuantity));
					BigDecimal originalAmount = item.getProduct().getPrice().multiply(BigDecimal.valueOf(item.getQuantity()));

					item.setDiscountedSubTotal(originalAmount.subtract(accumulatedDiscount));
				}
			}

		}
	}

	@Override
	public String getAppliesTo() {
		return appliesTo;
	}

	@Override
	public boolean isValid() {
		return LocalDate.now().isBefore(expiration);
	}

	private void validate(String appliesTo, int minimumQuantity, BigDecimal discount, LocalDate expiration) {
		Objects.requireNonNull(appliesTo, "appliesTo can't be null.");
		Objects.requireNonNull(discount, "discount can't be null.");
		Objects.requireNonNull(expiration, "expiration can't be null.");
		
		if (discount.compareTo(BigDecimal.ZERO) <= 0)
			throw new IllegalStateException("no point giving a '0' discount");

		if (minimumQuantity < 1)
			throw new IllegalArgumentException("minimumQuantity must be greater than 0.");

		if (!LocalDate.now().isBefore(expiration))
			throw new IllegalArgumentException("expiration must be later than today.");
	}

}
