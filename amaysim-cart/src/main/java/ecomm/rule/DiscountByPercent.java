package ecomm.rule;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import ecomm.cart.Item;

public class DiscountByPercent implements PromotionRule {

	private String appliesTo;

	private String promoCode;

	private LocalDate expiration;

	private int percent;

	// default validity of promotion expiration to 1 year if no expiration defined
	public DiscountByPercent(String appliesTo, int percent) {
		this(appliesTo, percent, LocalDate.now().plusYears(1));
	}

	public DiscountByPercent(String appliesTo, int percent, LocalDate expiration) {

		this.appliesTo = appliesTo;
		this.expiration = expiration;
		this.percent = percent;

		validate(this.appliesTo, this.percent, this.expiration);

	}

	public void applyTo(Item item) {
		if (isValid()) {

			Objects.requireNonNull(item, "item cannot be null.");

			if (appliesTo.equals(promoCode)) {

				BigDecimal discount = BigDecimal.valueOf(percent).divide(BigDecimal.valueOf(100));
				BigDecimal originalAmount = item.getProduct().getPrice();
				BigDecimal discountedAmount = originalAmount.subtract(originalAmount.multiply(discount));

				item.setDiscountedSubTotal(discountedAmount.multiply(BigDecimal.valueOf(item.getQuantity())));
			}
		}
	}

	@Override
	public String getAppliesTo() {
		return this.appliesTo;
	}

	@Override
	public boolean isValid() {
		return LocalDate.now().isBefore(expiration);
	}

	@Override
	public void promotionCode(String promoCode) {
		Objects.requireNonNull(promoCode, "promoCode can't be null.");

		this.promoCode = promoCode;
	}

	@Override
	public String getPromotionCode() {
		return this.promoCode;
	}

	private void validate(String appliesTo, int percent, LocalDate expiration) {

		Objects.requireNonNull(appliesTo, "appliesTo can't be null.");
		Objects.requireNonNull(expiration, "expiration can't be null.");

		if (percent <= 0 && percent > 100)
			throw new IllegalArgumentException("percent cannot be less than or equal to 0 and over 100.");

		if (!LocalDate.now().isBefore(expiration))
			throw new IllegalArgumentException("expiration must be later than today.");
	}
}
