package ecomm.rule;

import ecomm.cart.Item;

/**
 * Base promotion rule accepted by a Shopping cart implementation
 * 
 * @author rvillamor
 *
 */
public interface PromotionRule {

	/**
	 * Accepts the item to apply the rule if applicable
	 * 
	 * @param item
	 */
	void applyTo(Item item);

	/**
	 * Can be used by 'PromotionRule' implementations to check if rule is
	 * applicable to an item only or accross the board
	 * 
	 * @return can return item code or promotion code
	 */
	String getAppliesTo();

	boolean isValid();

	/**
	 * To set across the board promotios
	 * 
	 * @param promoCode
	 */
	public default void promotionCode(String promoCode) {
		// does nothing if not implemented :)
	}

	/**
	 * Can be used by across the board promotions, if not applicable to a
	 * certain product
	 * 
	 * @return promotional code if set
	 */
	public default String getPromotionCode() {
		return null;
	}

}
